EventSearch Android App

by Seid Redi

Description

Event search is an Android application that can be used to search music events by an artist

It uses the Spotify Api to collect information about a user's favourite artist and TicketMaster api to collect 
information about the events coming up.

It can also be used to search event by a specific artist.


